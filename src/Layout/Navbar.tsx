import React, { useEffect, useState } from "react";
import { SiEthereum } from "react-icons/si";
import "../style/main.scss";
import { InjectedConnector } from "@web3-react/injected-connector";
import { useWeb3React } from "@web3-react/core";

function Navbar() {
  const { active, account, activate, deactivate } = useWeb3React();
  const Injected = new InjectedConnector({
    supportedChainIds: [1, 3, 4, 5, 42],
  });

  // if wallet is already connected on refresh page it still keep holding same state
  useEffect(() => {
    Injected.isAuthorized().then((isAuthorized: boolean) => {
      if (isAuthorized) {
        connect();
      } else {
        console.log("try to connect wallet");
      }
    });
  }, []);

  async function connect() {
    try {
      activate(Injected);
    } catch (ex) {
      console.log(ex);
    }
  }

  return (
    <div className="Nav">
      <div className="Logo">
        <SiEthereum />
        Lottery
      </div>

      <p>
        <strong>Wallet_Address: </strong>
        {account}
      </p>
      {active ? (
        <button
          onClick={() => {
            connect();
          }}
        >
          Connected
        </button>
      ) : (
        <button
          onClick={() => {
            connect();
          }}
        >
          Connect
        </button>
      )}
    </div>
  );
}

export default Navbar;
