import "./App.css";
import Main from "./page/Main";
import Navbar from "./Layout/Navbar";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Main />
    </div>
  );
}

export default App;
